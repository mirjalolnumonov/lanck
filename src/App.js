import Main from "./pages/Home/Containers/Main"
import styles from './App.module.sass';

function App() {
  return (
    <div className={styles.App}>
        <section className={styles.content}>
            <Main />
        </section>
    </div>
  );
}

export default App;
