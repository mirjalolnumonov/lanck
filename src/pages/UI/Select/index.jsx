import { Select } from "antd";
import styles from "./Select.module.sass";
import "./SelectCustumize.sass";
import { useContext } from "react";
import Method from "../../../context/context";
import { methodTypes, types } from "../../../config/constants";
const { Option } = Select;

// I could create my own customized select with sass and js but I prefer to use ant design select as it was used in example site

const SelectBox = (props) => {
  const method = useContext(Method);
  return (
    <div className={styles.smallSelect}>
      <Select
        disabled={
          method === methodTypes.call && props.element.name === types.language
        }
        name={props.element.name}
        defaultValue={props.element.options[0]}
        style={{
          width: 80,
          marginRight: "15px",
        }}
        onChange={(event) => props.handleFormChange(event, props.element.name)}
      >
        {props.element.options.map((el, i) => {
          return (
            <Option key={i} value={el}>
              {el}
            </Option>
          );
        })}
      </Select>
    </div>
  );
};

export default SelectBox;
