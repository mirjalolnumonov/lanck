import styles from "./Button.module.sass";
import { createCurl } from "../../../utils";
import { buttonTypes } from "../../../config/constants";

const Button = (props) => {
  const clickCopyHandler = () => {
    const data = props.curl
      .map((el) => {
        return ` \\ -d ${el[0]}=${el[1]}`;
      })
      .join(" ");
    const text = createCurl(props.link, data);
    navigator.clipboard.writeText(text).then(() => {
      // in production usually here we implement some kind of popup
      alert("Saved to clipboard!");
    });
  };

  return (
    <button
      onClick={props.name === buttonTypes.Copy ? clickCopyHandler : null}
      className={`${styles[props.class]} ${
        props.name === buttonTypes.Request ? styles.buttonWithAnimation : ""
      }`}
      type={props.type}
    >
      {props.icon && (
        <i className={`${styles[props.icon]} ${styles.icon}`}> content </i>
      )}
      {props.name}
    </button>
  );
};

export default Button;
