import styles from "./Input.module.sass";
import { types } from "../../../config/constants";

const Input = (props) => {
  return (
    <input
      style={props.element.name === types.text ? { maxWidth: 300 } : null}
      name={props.element.name}
      key={props.element.name}
      onChange={(e) => props.handleFormChange(e.target.value, e.target.name)}
      className={`${styles.inputText} ${
        props.element.name !== types.number ? styles.inputWithAnimation : ""
      }`}
      placeholder={props.element.placeholder}
      value={props.element.value}
    />
  );
};

export default Input;
