import styles from "./Tab.module.sass";
const Tab = (props) => {
  return (
    // we also could use classnames package here
    <div
      onClick={() => props.handleClick(props.tab.id)}
      className={`${styles.tab} ${props.tab.active ? styles.active : null}`}
    >
      {props.tab.name}
    </div>
  );
};

export default Tab;
