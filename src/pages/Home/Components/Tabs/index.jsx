import Tab from "./Tab";
import styles from "./Tabs.module.sass";

const Tabs = (props) => {
  return (
    <div className={styles.tabs}>
      {props.tabs.map((el) => {
        return <Tab key={el.id} handleClick={props.handleClick} tab={el} />;
      })}
    </div>
  );
};

export default Tabs;
