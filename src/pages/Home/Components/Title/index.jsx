import styles from "./Title.module.sass";
const Title = () => {
  return <div className={styles.title}>Try the API</div>;
};

export default Title;
