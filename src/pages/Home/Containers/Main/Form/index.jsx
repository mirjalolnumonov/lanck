import Input from "../../../../UI/Input";
import Button from "../../../../UI/Button";
import styles from "./Form.module.sass";
import SelectBox from "../../../../UI/Select";

const Form = (props) => {
  return (
    <form className={styles.form} onSubmit={props.handleSubmit}>
      {props.fields.map((el, i) => {
        switch (el.type) {
          case "input":
            return (
              <Input
                key={i}
                handleFormChange={props.handleFormChange}
                element={el}
              />
            );
          case "select":
            return (
              <SelectBox
                key={i}
                element={el}
                handleFormChange={props.handleFormChange}
              />
            );
          default:
            console.log("Type is not found");
        }
      })}
      <Button type="submit" name="Request" class="success" />
    </form>
  );
};
export default Form;
