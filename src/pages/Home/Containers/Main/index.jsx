import Button from "../../../UI/Button";
import List from "./List";
import Title from "../../Components/Title";
import Tabs from "../../Components/Tabs";
import { useEffect, useState } from "react";
import Form from "./Form";
import initialTabsState from "../../../../assets/data/data.json";
import styles from "./Main.module.sass";
import Method from "../../../../context/context";
import { types } from "../../../../config/constants";

const Main = () => {
  const [tabs, setTabs] = useState(initialTabsState);
  const [activeTab, setActiveTab] = useState(initialTabsState[0]);
  const [curl, setCurl] = useState([]);
  const [method, setMethod] = useState("");

  useEffect(() => {
    const curlData = activeTab.formElements.map((el) => {
      const val =
        el.value && el.name !== types.text
          ? el.value
          : el.value && el.name === types.text
          ? '"' + el.value + '"'
          : '""';
      return [el.name, val];
    });
    setCurl(curlData);
  }, [activeTab.formElements]);

  const handleTabClick = (id) => {
    const newTabs = initialTabsState.map((el) => {
      el.id === id ? (el.active = true) : (el.active = false);
      return el;
    });
    setTabs(newTabs);
    setActiveTab(initialTabsState.find((el) => el.id === id));
  };
  const handleFormChange = (value = "", name = "") => {
    if (name === types.method) setMethod(value);
    const formElements = activeTab.formElements.map((el) => {
      if (el.name === name) {
        el.value = value;
      }
      return el;
    });
    setActiveTab({
      ...activeTab,
      formElements: formElements,
    });
  };
  const handleSubmit = (event) => {
    event.preventDefault();
    // here we can implement form validation and request to the API
    console.log(activeTab.formElements);
  };

  return (
    <>
      <Title />
      <Tabs tabs={tabs} handleClick={handleTabClick} />
      <Method.Provider value={method}>
        <Form
          handleSubmit={handleSubmit}
          handleFormChange={handleFormChange}
          fields={activeTab.formElements}
        />
      </Method.Provider>
      <section className={styles.main}>
        <List link={activeTab.link} curl={curl} />
        <div className={styles.curlButtons}>
          <Button
            link={activeTab.link}
            curl={curl}
            type="button"
            class="default"
            name="Copy"
            icon="copy"
          />
          <Button type="button" class="default" name="Reference" icon="code" />
        </div>
      </section>
    </>
  );
};

export default Main;
