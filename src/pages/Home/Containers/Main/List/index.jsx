import styles from "./List.module.sass";
import { baseurl, token } from "../../../../../config/constants";
const List = (props) => {
  return (
    <div className={styles.curlPanel}>
      <ul>
        <li key={props.link}>
          <i>curl -XPOST </i>
          <span>{baseurl + props.link}</span> <i> \ </i>
        </li>
        <li className={styles.withLeft} key={token.substring(0, 4)}>
          <i>-u </i>
          <b className={styles.token}>{token}</b>
          <i> \ </i>
        </li>
        {props.curl.map((el, i) => {
          return (
            <li className={styles.withLeft} key={i}>
              <i>-d </i>
              {el[0]}=<b>{el[1]}</b>
              {i !== props.curl.length - 1 && <i> \ </i>}
            </li>
          );
        })}
      </ul>
    </div>
  );
};

export default List;
