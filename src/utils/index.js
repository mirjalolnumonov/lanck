import { baseurl, token } from "../config/constants";

export function createCurl(link, params) {
  return `curl -XPOST ${baseurl}${link} \\ -u ${token} ${params.toString()}`;
}
